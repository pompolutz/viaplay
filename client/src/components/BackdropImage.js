import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { IMAGE_BASE_URL, BACKGROP_SIZE } from "../constants";

function BackdropImage({ show, item }) {
    return (
        <ImageContainer show={show}>
            <GradientImage
                src={`${IMAGE_BASE_URL}${BACKGROP_SIZE}${item.backdrop_path}`}
            />
        </ImageContainer>
    );
}

BackdropImage.propTypes = {
    show: PropTypes.bool.isRequired,
    item: PropTypes.object.isRequired,
};

const ImageContainer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 750px;
    opacity: ${(props) => (props.show ? 0.4 : 0)};
    transition: opacity 1s ease-in 0.5s;
`;

const GradientImage = styled.img`
    -webkit-mask-image: linear-gradient(
        to bottom,
        black 10%,
        rgba(0, 0, 0, 0) 65%
    );
    mask-image: linear-gradient(to bottom, black 10%, rgba(0, 0, 0, 0) 65%);
    width: 100%;
`;

export default BackdropImage;
