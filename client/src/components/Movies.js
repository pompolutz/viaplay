import React from "react";
import styled from "styled-components";
import axios from "axios";
import MovieItem from "./MovieItem";
import BackdropImage from "./BackdropImage";
import { IMAGE_BASE_URL, BACKGROP_SIZE } from "../constants";

class Movies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            content: [],
            cursorRow: 0,
            cursorCol: [],
            ids: [],
            selectedId: null,
        };

        this.handleRemoteKey = this.handleRemoteKey.bind(this);
    }

    async componentDidMount() {
        const movies = await axios.get("/api/movies");
        const shows = await axios.get("/api/tvshows");

        this.setState({
            content: [
                {
                    sectionTitle: "Most Watchable Now",
                    items: movies.data.results,
                },
                {
                    sectionTitle: "Most Popular On TV",
                    items: shows.data.results,
                },
            ],
            cursorCol: [0, 0],
        });

        window.addEventListener("keydown", this.handleRemoteKey);
    }

    handleRemoteKey(e) {
        const { key } = e;
        switch (key) {
            case "ArrowUp":
                this.setState(({ cursorRow }) => {
                    const nextCursorRow = cursorRow > 0 ? cursorRow - 1 : 0;
                    const root = document.getElementById("root");
                    root.scrollIntoView({ behavior: "smooth" });

                    return {
                        cursorRow: nextCursorRow,
                    };
                });
                break;
            case "ArrowLeft":
                this.setState(({ cursorRow, cursorCol }) => {
                    const lastCursorPosition = cursorCol[cursorRow];
                    const nextCursorCol =
                        lastCursorPosition > 0 ? lastCursorPosition - 1 : 0;
                    return {
                        cursorCol: [
                            ...cursorCol.slice(0, cursorRow),
                            nextCursorCol,
                            ...cursorCol.slice(cursorRow + 1),
                        ],
                    };
                });
                break;
            case "ArrowRight":
                this.setState(({ cursorRow, content, cursorCol }) => {
                    const columnsInCurrentRow =
                        content && content[cursorRow].items.length;
                    const lastCursorPosition = cursorCol[cursorRow];
                    const nextCursorCol =
                        lastCursorPosition < columnsInCurrentRow - 1
                            ? lastCursorPosition + 1
                            : columnsInCurrentRow - 1;
                    return {
                        cursorCol: [
                            ...cursorCol.slice(0, cursorRow),
                            nextCursorCol,
                            ...cursorCol.slice(cursorRow + 1),
                        ],
                    };
                });
                break;
            case "ArrowDown":
                this.setState(({ cursorRow, content }) => {
                    const nextCursorRow =
                        cursorRow < content.length - 1
                            ? cursorRow + 1
                            : content.length - 1;

                    if (nextCursorRow > 0) {
                        const section = document.getElementById(
                            `section_${nextCursorRow}`
                        );
                        section.scrollIntoView({ behavior: "smooth" });
                    }

                    return {
                        cursorRow: nextCursorRow,
                    };
                });
                break;
            default:
                break;
        }
    }

    render() {
        const { content, cursorRow, cursorCol } = this.state;
        const selectedItem =
            content.length > 0 &&
            content[cursorRow].items[cursorCol[cursorRow]];
        return (
            <Root
                backgroundImagePath={`${IMAGE_BASE_URL}${BACKGROP_SIZE}${selectedItem.backdrop_path}`}
            >
                {this.state.content.map((c, row) => {
                    return c.items.map((item, col) => (
                        <BackdropImage
                            key={item.id}
                            show={
                                row === this.state.cursorRow &&
                                col === this.state.cursorCol[row]
                            }
                            item={item}
                        />
                    ));
                })}
                <Section></Section>
                {this.state.content.map((c, row) => (
                    <Section id={`section_${row}`} key={row}>
                        <SectionContentContainer>
                            <SectionTitle>{c.sectionTitle}</SectionTitle>
                            <ContentPanel
                                offsetBy={this.state.cursorCol[row]}
                                style={{ transition: ".75s all ease-out" }}
                            >
                                {c.items.map((item, col) => (
                                    <MovieItem
                                        selected={
                                            row === this.state.cursorRow &&
                                            col === this.state.cursorCol[row]
                                        }
                                        key={item.id}
                                        {...item}
                                    />
                                ))}
                            </ContentPanel>
                        </SectionContentContainer>
                    </Section>
                ))}
            </Root>
        );
    }
}

const Root = styled.div`
    flex: 1 0;
    background: black;
    display: flex;
    flex-flow: column;
    position: relative;
`;

const Section = styled.section`
    flex: 1 0;
    display: flex;
    box-sizing: border-box;
    margin-bottom: 1rem;
    padding-left: 2rem;
`;

const SectionContentContainer = styled.div`
    position: relative;
    flex-grow: 1;
    overflow: visible;
`;

const ContentPanel = styled.div`
    position: absolute;
    display: flex;
    left: ${(props) => `${props.offsetBy * -(200 + 16)}px`};
`;

const SectionTitle = styled.h1`
    color: white;
    margin-bottom: 12px;
    user-select: none;
`;

export default Movies;
