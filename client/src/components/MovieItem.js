// eslint-disable-next-line
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { IMAGE_BASE_URL, POSTER_SIZE } from "../constants";

const MovieItem = styled.div`
    flex: 1 0 auto;
    width: 200px;
    height: 280px;
    margin: 1rem 1rem 0 0;
    opacity: ${(props) => (props.selected ? 1 : 0.8)};
    user-select: none;
    background-image: ${(props) =>
        `url(${IMAGE_BASE_URL}${POSTER_SIZE}${props.poster_path})`};
    background-size: cover;
    background-position: center center;
    transform: ${(props) => (props.selected ? `scale(1.1)` : `scale(1)`)};
    transition: 0.5s all ease-out;
`;

MovieItem.propTypes = {
    id: PropTypes.number.isRequired,
    poster_path: PropTypes.string,
    selected: PropTypes.bool,
};

export default MovieItem;
