import React from "react";
import styled from "styled-components";
import Movies from "./components/Movies";

export default function App() {
    return (
        <ContentWrapper>
            <Header>
                <NavigationContainer>
                    <Logo
                        src="/header-logo-large.png"
                        alt="viaplay-logo-large"
                    />
                </NavigationContainer>
            </Header>
            <MainContentContainer>
                <Movies />
            </MainContentContainer>
            <Footer></Footer>
        </ContentWrapper>
    );
}

const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 1280px;
`;

const Header = styled.header`
    background: #0f161a;
    border-top: 1px solid #0f161a;
    height: 64px;
    position: relative;
`;

const NavigationContainer = styled.div`
    margin: 0 80px;
`;

const Logo = styled.img`
    width: 168px;
    margin-top: 15px;
`;

const MainContentContainer = styled.div`
    flex-grow: 1;
    width: calc(100% - 160px);
    display: flex;
    margin: 0 auto;
`;

const Footer = styled.footer`
    background: #0f161a;
    border-bottom: 1px solid #0f161a;
    height: 64px;
`;
