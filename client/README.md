## How To Run

This application depends on server application already running on localhost:8080.
If you have changed port for the server application, please change the **proxy** property in package.json.

## Available Scripts

## Create React App Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Additional Scripts

### `prettify`

Run prettier against ./src folder

### `precommit`

Run eslint and then prettier

---

### `husky is watching you!`

Note, that every commit is guarded by loyal husky, which should bark if you violate eslint but also be a good doggo and prettify things for you.
