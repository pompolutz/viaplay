# Welcome To Viaplay on a Big Screen!


Taking into account thats the main focus of this home assesment I've made a small prototype
of what I can imagine to be a mini version of TV-first app, with following decisions:

#### CLIENT
- I have decided to use restricted sizes rather than go more fluid, for the sake of simplifying math and concentrating on idea;
- I have concentrated on exploring how it should work with arrow keys only;
- I have decided in the end to skip on mouse interactions because, even though I had some ideas, it would 
just require more time. But I have enjoyed the exploration of a concept up to this point and think it would have been interesting
to go further;
- I had time only to verify on my Windows 10 laptop in Chrome 81
- I have see little value of spending time on tests in one time exploration of a concept project, but they sure will be valuable for real product. I would have use Jest then.

#### SERVER
- Because I have decided not to visualize videos on client end, but still use server there for supplying with data, 
I have decided to make this API project to be just a proxy for TMDB. 
- I have commited .env file for the sake of saving your time, but I would have ignored that file in real life
- Again, I was so tired in the end, so I decided to not spend time on end-to-end testing, but I have considered making it with Jest and supertest.


## Two ways to see the result

### Netlify

I have published last build of the client to Netlify: https://friendly-kirch-b8e5b5.netlify.app/
This version of the client talks to TMDB directly via **moviedb-promise** API wrapping library.

I have noticed that to see the intended result I have to zoom out my Chrome 81 (Window 10) to 67%.

### Running locally

You need to run `yarn start` in server folder first and then in client folder too.
