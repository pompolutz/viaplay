import { Router } from "express";
import moviesController from "./movies";
import tvshowsController from "./tvshows";

const router = new Router();
router.use("/movies", moviesController);
router.use("/tvshows", tvshowsController);

export default router;
