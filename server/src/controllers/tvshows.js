import { Router } from "express";
import { getTvShowsAsync } from "../services";

const router = new Router();

router.get("/", async (req, res) => {
    try {
        const movies = await getTvShowsAsync();
        return res.json(movies);
    } catch(error) {
        console.log(error.message);
        return res.status(401).end(error.message);
    }
});

export default router;