import { Router } from "express";
import { getMoviesAsync } from "../services";

const router = new Router();

router.get("/", async (req, res) => {
    try {
        const movies = await getMoviesAsync();
        return res.json(movies);
    } catch(error) {
        console.log(error.message);
        return res.status(401).end(error.message);
    }
});

export default router;
