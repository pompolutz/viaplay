import { MovieDb } from "moviedb-promise";
import dotenv from "dotenv";

dotenv.config();

const movieDb = new MovieDb(process.env.TMDB_KEY);

export const getMoviesAsync = () => movieDb.miscPopularMovies();
export const getTvShowsAsync = () => movieDb.miscPopularTvs();
