import express from "express";
import controllers from "./controllers";
import cors from "cors";

const app = express();

app.use(cors());
app.use("/api", controllers);

export default app;
