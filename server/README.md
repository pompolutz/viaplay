## Available Scripts

### `yarn`

To get started, please install dependecies using yarn.

### `yarn start`

Will start local server with nodemon on port specified via .env file.

---


### `husky is watching you!`

Note, that every commit is guarded by loyal husky, which should bark if you violate eslint but also be a good doggo and prettify things for you.

